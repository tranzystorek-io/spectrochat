# spectrochat

## About

Simple configuration for hosting
an instance of [thelounge](https://thelounge.chat/) on [fly.io](https://fly.io/).

## TheLounge config

Here are some notable configuration options to set.
On a running instance you can do `fly ssh console` and modify them in the config file, by default `/var/opt/thelounge/config.js`.

| Setting | Value | Comments |
|---------|-------|----------|
| [`host`](https://thelounge.chat/docs/configuration#host) | `"0.0.0.0"` | Containerized fly.io apps should bind to this fixed address |
| [`reverseProxy`](https://thelounge.chat/docs/configuration#reverseproxy) | `true` | Fly.io sets up a basic rev proxy for web apps |
